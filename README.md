Uniti ARC

Eagle files of the Uniti ARC. Derived from The Arduino UNO files (available at: 
https://www.arduino.cc/en/Main/ArduinoBoardUno).

Released under a Creative Commons Attribution-ShareAlike license.
