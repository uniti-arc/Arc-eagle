x = 0:1/100:4*pi+0.01;
s1= cos(x);
s2= cos(x+2/3*pi);
s3= cos(x+4/3*pi);
s1n =s1 .* x.^2;
s2n =s2 .* x.^2;
s3n =s3 .* x.^2;
plot(x,[s1n;s2n;s3n], 'k', 'LineWidth',4)